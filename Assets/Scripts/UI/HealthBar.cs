﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour {
    // credit: https://www.youtube.com/watch?v=ox-QiHfUqdI
    [SerializeField]
    private Image healthBar;

    [SerializeField]
    private float healthFillAmount;

    [SerializeField]
    private GameObject player;

    private HitReaction playerHitReaction;
    private float healthRegen;

	// Use this for initialization
	void Start () {
        healthBar.fillAmount = 1;
        healthFillAmount = 1;
        healthRegen = 0.001f;
        playerHitReaction = player.GetComponent<HitReaction>();
	}
	
    /**
     * deducts health by 1/3 on every hit
     * if health reaches below a total of 1/3, set it to zero
     */
    public void deductHealth()
    {
        if (healthFillAmount < 0.33)
        {
            healthFillAmount = 0;
        }
        else
        {
            healthFillAmount -= 1f / 3f;
        }
    }

    /**
     * let some other object check if health is zero
     */
    public bool isZeroHealth()
    {
        return healthBar.fillAmount <= 0.05;
    }

    /**
     * update health value on UI
     */
    private void updateHealth()
    {
       if(healthFillAmount > 1f)
        {
            healthFillAmount = 1f;
        }
        else if(healthFillAmount < 1f)
        {
            healthFillAmount += healthRegen;
        }
        healthBar.fillAmount = healthFillAmount;
    }

	// Update is called once per frame
	void Update () {
        if (Time.timeScale == 1)
        {
            updateHealth();
        }
	}

    public void Revive()
    {
        healthFillAmount = 1f;
    }
}
