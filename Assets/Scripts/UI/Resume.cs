﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Resume : MonoBehaviour {

    private Button resumeButton;
    private GameObject pauseMenuUI;

	// Use this for initialization
	void Start () {
        resumeButton = GetComponent<Button>();
        resumeButton.onClick.AddListener(resumeTask);

        pauseMenuUI = GetComponentInParent<Canvas>().gameObject;
	}
	
    void resumeTask()
    {
        Time.timeScale = 1;
        pauseMenuUI.SetActive(false);
    }
}
