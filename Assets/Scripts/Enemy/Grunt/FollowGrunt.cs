﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowGrunt : Grunt {

    public Transform player;
    public float distanceTolerance;
    bool setDistance;
    Rigidbody2D rb;

	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody2D>();
        setDistance = false;
        Init();
        direction = 0;
	}
	
	/**
     * if the player is within a certain range of this enemy, the enemy will rush towards the player
     */
	void Update () { 
        float distanceBetween = Mathf.Abs(transform.position.x - player.position.x);

        // only set the distance once
        if(distanceBetween <= distanceTolerance && !setDistance)
        {
            setDistance = true;
            direction = (int) (player.position.x - transform.position.x) / (int) distanceBetween;
        }
        else if(distanceBetween > distanceTolerance && setDistance)
        {
            setDistance = false;
            direction = 0;
        }
        Move(rb);
    }
}
