﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleGrunt : Grunt {

    Rigidbody2D rb;

	// Use this for initialization
	void Start () {
        Debug.Log("This is a simple grunt");
        rb = GetComponent<Rigidbody2D>();
        Init();
    }

    // Update is called once per frame
    void Update () {
        Move(rb);
	}

    private void OnCollisionEnter2D(Collision2D collision)
    {
        FlipDirection(collision.collider);
    }
}
