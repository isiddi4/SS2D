﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grunt : MonoBehaviour {

    public float speed;
    public int direction;

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		
	}
    
    /**
     * initialize the grunt
     */ 
    protected void Init()
    {
        if (speed == 0)
        {
            speed = 2;
        }

        if (direction == 0)
        {
            direction = 1;
        }
    }

    /**
     * if the grunt hits an obstacle, flip the direction of movement
     */
    protected void FlipDirection(Collider2D other)
    {
        if(other.tag == "obstacle")
        {
            direction *= -1;
        }
    }

    /**
     * move the given rigidbody by giving it a velocity
     */
    protected void Move(Rigidbody2D rb)
    {
        try
        {
            rb.velocity = new Vector2(speed * direction, 0);
        }
        catch (System.NullReferenceException e)
        {
            Debug.Log(e, this);
        }
    }
}
