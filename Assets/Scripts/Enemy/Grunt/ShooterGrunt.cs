﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShooterGrunt : Grunt {

    public GameObject projectile;
    float projectileSpeed;
    int projectileCooldown;
    Vector3 offset;
    bool canShoot;
    Rigidbody2D rb;

	// Use this for initialization
	void Start () {
        Debug.Log("This is a grunt that can shoot");
        rb = GetComponent<Rigidbody2D>();
        offset = new Vector3(0.4f, 0f, 0f);
        canShoot = true;
        projectileCooldown = 2;
        Init();
        projectileSpeed = 2f * speed;
	}
	
	/*
     * this enemy shoots projectiles as it walks
     */
	void Update () {
        Move(rb);
        if(canShoot)
        {
            GameObject shotProjectile = Instantiate(projectile, transform.position + offset * direction, Quaternion.identity);
            shotProjectile.GetComponent<Rigidbody2D>().velocity = new Vector2(projectileSpeed * direction, 0f);
            StartCoroutine(Shoot());
        }
	}

    /**
     * prohibit the enemy from shooting during the cooldown duration
     */
    IEnumerator Shoot()
    {
        canShoot = false;
        yield return new WaitForSeconds(projectileCooldown);
        canShoot = true;
    }

    /**
     * turn around on hitting a wall of some sort 
     */
    private void OnCollisionEnter2D(Collision2D collision)
    {
        FlipDirection(collision.collider);
    }
}
