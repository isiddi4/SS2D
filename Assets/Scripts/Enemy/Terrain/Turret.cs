﻿using UnityEngine;
using System.Collections;

public class Turret : MonoBehaviour {

    public Transform player;
    public GameObject projectile;
    public float distanceTolerance;
    public float projectileSpeed;
    int projectileCooldown;
    Vector3 offset;
    bool canShoot;
    Rigidbody2D rb;

    // Use this for initialization
    void Start() {
        rb = GetComponent<Rigidbody2D>();
        offset = new Vector3(0.4f, 0f, 0f);
        canShoot = true;
        projectileCooldown = 2;
    }

    /**
     * if the player is within a certain range of the turret, the turret will fire a projectile at the player
     */
    void Update() {
        if (player.gameObject.activeSelf)
        {
            float distanceBetween = Mathf.Abs(transform.position.x - player.position.x);
            float direction = (int)(player.position.x - transform.position.x) / (int)distanceBetween;
            if (distanceBetween <= distanceTolerance)
            {
                if (canShoot)
                {
                    GameObject shotProjectile = Instantiate(projectile, transform.position + offset * direction, Quaternion.identity);
                    shotProjectile.GetComponent<Rigidbody2D>().velocity = new Vector2(projectileSpeed * direction, 0f);
                    StartCoroutine(Shoot());
                }
            }
        }
    }

    /**
     * prohibit the enemy from shooting during the cooldown duration
     */
    IEnumerator Shoot()
    {
        canShoot = false;
        yield return new WaitForSeconds(projectileCooldown);
        canShoot = true;
    }
}
