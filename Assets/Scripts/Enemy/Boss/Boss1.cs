﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss1 : MonoBehaviour {

    public GameObject player;
    public float speed;
    public float jumpForce;
    Rigidbody2D rb;

	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody2D>();
	}
	
	/**
     * jump towards the player
     */
	void Update () {
        if (player != null)
        {
            float distanceBetween = Mathf.Abs(transform.position.x - player.transform.position.x);
            int direction = 0;
            if ((int)distanceBetween != 0)
            {
                direction = (int)(player.transform.position.x - transform.position.x) / (int) distanceBetween;
            }
            rb.velocity = new Vector2(direction * speed, rb.velocity.y);
        }
    }

    /**
     * if the player and the boss are on the ground at the same time, destroy the player
     */
    private void OnCollisionStay2D(Collision2D collision)
    {
        if (player != null)
        {
            if (collision.collider.tag == "ground")
            {
                rb.AddForce(new Vector2(0, jumpForce));
                if (player.transform.position.y <= transform.position.y)
                {
                    Debug.Log("Boss: " + transform.position.y);
                    Debug.Log("Player: " + player.transform.position.y);
                    Destroy(player);
                }
            }
        }
    }
}
