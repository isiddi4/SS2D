﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Miniboss1 : MonoBehaviour {

    public GameObject bomb;
    public float jumpForce;
    public float xSpeed;
    public int direction;
    Vector3 offset;
    float gravitySpeed;
    float initialYPos;
    bool inAir;
    Rigidbody2D rb;

	// Use this for initialization
	void Start () {
        offset = new Vector3(0f, -1f, 0f);
        gravitySpeed = Physics2D.gravity.magnitude;
        initialYPos = transform.position.y;
        inAir = false;
        rb = GetComponent<Rigidbody2D>();

        if(jumpForce == 0)
        {
            jumpForce = 100;
        }

        if(xSpeed == 0)
        {
            xSpeed = 2;
        }

        if(direction == 0)
        {
            direction = 1;
        }
	}
	
	// Update is called once per frame
	void Update () {
        rb.velocity = new Vector2(xSpeed * direction, rb.velocity.y);
        float verticalSpeed = Mathf.Abs(rb.velocity.y);
        if(verticalSpeed > 0 && verticalSpeed <= 0.2)
        {
            DropBomb();
        }
	}

    void DropBomb()
    {
        Instantiate(bomb, transform.position + offset, Quaternion.identity);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.collider.tag == "obstacle")
        {
            direction *= -1;
        }
    }

    private void OnCollisionStay2D(Collision2D collision)
    {
        if(collision.collider.tag == "ground")
        {
            rb.AddForce(new Vector2(0f, jumpForce));
        }
    }
}
