﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Collectable : MonoBehaviour {

    [SerializeField]
    private GameObject player;

    protected PlayerAttack playerAttack;

    void Start()
    {
        // we need this so we can add the effect the player
        playerAttack = player.GetComponent<PlayerAttack>();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.CompareTag("Player"))
        {
            AddEffect();

            // make sure it can't be collected again
            Destroy(gameObject);
        }
    }

    public abstract void AddEffect();
}
