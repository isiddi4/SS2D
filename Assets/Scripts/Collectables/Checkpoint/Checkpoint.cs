﻿using UnityEngine;
using System.Collections;

public class Checkpoint : Collectable {
    [SerializeField]
    private Transform respawnPoint;

    public override void AddEffect()
    {
        respawnPoint.position = transform.position;
    }
}
