﻿using UnityEngine;
using System.Collections;

public class RockPower : Collectable {

    public override void AddEffect()
    {
        playerAttack.setRockPower();
    }
}
