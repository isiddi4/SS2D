﻿using UnityEngine;
using System.Collections;

public class FirePower : Collectable {

    public override void AddEffect()
    {
        playerAttack.setFirePower();
    }
}
