﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Move : MonoBehaviour {

	Rigidbody2D rb;
	public float speed;
	bool canClimb;

	// Use this for initialization
	void Start () {
		rb = GetComponent<Rigidbody2D> ();
		canClimb = false;
	}

	void GetInput() {
		if (Input.GetButton("Horizontal")) {
			float direction = Input.GetAxisRaw ("Horizontal");
			rb.velocity = new Vector2 (speed * direction, rb.velocity.y);
		}

		if (Input.GetButtonDown("Jump") && !canClimb && rb.velocity.y == 0f) {
			rb.AddForce (new Vector2 (0, 300));
			print ("Jump");
		}

		if (Input.GetButton ("Jump") && canClimb) {
			rb.velocity = new Vector2 (0f, speed);
		}
	}

	void OnTriggerEnter2D(Collider2D other) {
		if (other.name == "Climbable") {
			canClimb = true;
			print ("can climb");
		}
	}

	void OnTriggerExit2D(Collider2D other) {
		if (other.name == "Climbable") {
			canClimb = false;
			print ("can't climb");
		}
	}

	// Update is called once per frame
	void Update () {
        if (Time.timeScale == 1)
        {
            GetInput();
        }
        
	}
}
