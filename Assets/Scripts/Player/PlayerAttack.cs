﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class PlayerAttack : MonoBehaviour
{
    private const int NONE_ID = 0;
    private const int FIRE_ID = 1;
    private const int ROCK_ID = 2;

    private Vector3 offset;
    private int powerUpID;
    private float direction;

    [SerializeField]
    private GameObject projectile;

    [SerializeField]
    private GameObject rock;

    private KeyCode AttackKey;

    // Use this for initialization
    void Start()
    {
        offset = new Vector3(0.4f, 0f, 0f);
        powerUpID = 0;
        direction = 0;
        AttackKey = KeyCode.K;
    }

    private void NoPowerUp()
    {
        if(Input.GetKeyDown(AttackKey))
        {
            // do nothing
        }
    }

    /**
     * Instantiate something to attack with
     * @param verticalVelocity - (float) how fast the object should be shot upwards
     * @param toShoot - (GameObject) gameObject to fire
     */
    private void FireProjectile(float verticalVelocity, GameObject toShoot)
    {
        GameObject shotProjectile = Instantiate(toShoot, transform.position + offset * direction, Quaternion.identity);
        shotProjectile.GetComponent<Rigidbody2D>().velocity = new Vector2(4 * direction, verticalVelocity);
    }

    /**
     * fire a projectile that moves horizontally
     */ 
    private void FirePowerUp()
    {
        if (Input.GetKeyDown(AttackKey))
        {
            FireProjectile(0f, projectile);
        }
    }

    /**
     * fire a projectile that moves in an arc
     */
    private void RockPowerUp()
    {
        if (Input.GetKeyDown(AttackKey))
        {
            FireProjectile(15f, rock);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.timeScale == 1)
        {
            GetInput();
        }
        switch (powerUpID)
        {
            case NONE_ID:
                NoPowerUp();
                break;
            case FIRE_ID:
                FirePowerUp();
                break;
            case ROCK_ID:
                RockPowerUp();
                break;
            default:
                print("error in powerup switch");
                NoPowerUp();
                break;
        }
    }

    void GetInput()
    {
        if (Input.GetAxisRaw("Horizontal") != 0)
        {
            direction = 1f * Input.GetAxisRaw("Horizontal");
        }
    }

    public void setFirePower()
    {
        powerUpID = FIRE_ID;
    }

    public void setRockPower()
    {
        powerUpID = ROCK_ID;
    }
}
