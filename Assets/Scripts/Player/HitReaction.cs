﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HitReaction : MonoBehaviour {
    private bool isHit;

    private static float ENEMY_LAYER = 8;
    private static float BOSS_LAYER = 10;

    [SerializeField]
    private Image healthBarImage;

    private HealthBar healthBar;

    // Use this for initialization
    void Start()
    {
        healthBar = healthBarImage.GetComponent<HealthBar>();
        isHit = false;
    }

    private void updatePlayerHealth()
    {
        if (isHit)
        {
            print("start deduct health");
            healthBar.deductHealth();
            isHit = false;
        }
    }

    // Update is called once per frame
    void Update () {
        updatePlayerHealth();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        int hitLayer = collision.gameObject.layer;
        if(hitLayer == ENEMY_LAYER || hitLayer == BOSS_LAYER)
        {
            isHit = true;
            print("hit");
        }
    }
}
