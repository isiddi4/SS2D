﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Background : MonoBehaviour {

    [SerializeField]
    private GameObject pauseMenuUI;

	// Use this for initialization
	void Start () {

        // ignore collision between enemies
        Physics2D.IgnoreLayerCollision(8, 8);

        // ignore collision between enemies and boss
        Physics2D.IgnoreLayerCollision(8, 10);

        Physics2D.IgnoreLayerCollision(9, 9);

        // ignore collision between player and boss
        Physics2D.IgnoreLayerCollision(9, 10);
    }
	
	// Update is called once per frame
	void Update () {
        PauseGame();
	}

    void PauseGame()
    {
        if (Input.GetKeyDown(KeyCode.Escape) && !pauseMenuUI.activeInHierarchy)
        {
            pauseMenuUI.SetActive(true);
            Time.timeScale = 0;
        }
    }
}
