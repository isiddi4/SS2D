﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DeathHandler : MonoBehaviour {

    [SerializeField]
    private GameObject player;

    [SerializeField]
    private Image healthBarImage;

    private HealthBar healthBar;

    [SerializeField]
    private Transform respawnPoint;

    [SerializeField]
    private float deathCooldown;

    void Start()
    {
        healthBar = healthBarImage.GetComponent<HealthBar>();
    }

    void Update()
    { 
        if(healthBar.isZeroHealth())
        {
            Execute();
            healthBar.Revive();
        }
    }

    private void Execute()
    {
        StartCoroutine(Respawn());
    }

    IEnumerator Respawn()
    {
        player.SetActive(false);
        yield return new WaitForSeconds(deathCooldown);
        player.SetActive(true);
        player.transform.position = respawnPoint.position;
    }
}
