﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileFriend : MonoBehaviour {

    Rigidbody2D rb;

	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody2D>();

        // don't let the projectile fall
        rb.gravityScale = 0;
        gameObject.layer = 9;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnCollisionEnter2D(Collision2D collision)
    {
        // if the projectile collides with anything, destroy the instance of the projectile
        Destroy(gameObject);
    }
}
